import os
import schedule

from git import Repo, InvalidGitRepositoryError
from pathlib import Path
import shutil
import logging


class Repos:
	url = "https://github.com/ceryliae/DnDAppFiles.git"
	dir_name = "dnd"

	logger = logging.getLogger()

	def __init__(self, update_callback):
		self.update_callback = update_callback
		self.logger.debug("Starting repo initialisation...")
		os.chdir(os.path.dirname(__file__))
		self.path = os.getcwd() + "/" + self.dir_name
		self.logger.debug("Repo path: %s", self.path)
		if not Path(self.path).is_dir():
			self.logger.debug("Repo doesn't exist, cloning...")
			os.makedirs(self.path)
			self.__create_repo()
		else:
			try:
				self.repo = Repo(path=self.path)
				self.origin = self.repo.remotes.origin
				self.logger.debug("Repo looks good, updating...")
				self.update()
			except InvalidGitRepositoryError:
				self.logger.debug("There is some shit in your dir, recreating...")
				self.__create_repo()
		self.logger.debug("Repo init finished.")
		schedule.every().day.do(self.update)

	def __create_repo(self):
		for file in os.listdir(self.path):
			shutil.rmtree(file)
		self.repo = Repo.clone_from(url=self.url, to_path=self.path)
		self.origin = self.repo.remotes.origin
		self.update_callback()

	def update(self):
		self.logger.debug("Starting repo update")
		self.origin.pull()
		self.update_callback()
