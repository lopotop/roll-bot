import logging
import os
import xml.etree.ElementTree as ET
from fuzzywuzzy import fuzz
from operator import itemgetter


class Parser:
	logger = logging.getLogger()

	os.chdir(os.path.dirname(__file__))
	base_path = os.path.join(os.getcwd(), "dnd")

	spell_dir = os.path.join(base_path, "Spells")
	item_dir = os.path.join(base_path, "Items")
	monsters_dir = os.path.join(base_path, "Bestiary")

	spells = []
	items = []
	monsters = []

	def update(self):
		spell_files = map(lambda file: os.path.join(self.spell_dir, file), os.listdir(self.spell_dir))
		item_files = map(lambda file: os.path.join(self.item_dir, file), os.listdir(self.item_dir))
		monster_files = map(lambda file: os.path.join(self.monsters_dir, file), os.listdir(self.monsters_dir))

		self.spells = self.__create_list(spell_files)
		self.items = self.__create_list(item_files)
		self.monsters = self.__create_list(monster_files)

	@staticmethod
	def find(string, some_list):
		scores = []
		string = string.lower()
		for i in range(len(some_list)):
			scores.append([i,
			               fuzz.partial_ratio(some_list[i][0].text.lower(), string),
			               fuzz.ratio(some_list[i][0].text.lower(), string)])
		sorted_scores = sorted(scores, key=itemgetter(1, 2), reverse=True)
		best_matches = []
		i = 0
		while True:
			current_score = sorted_scores[i]
			best_matches.append(current_score[0])
			i = i + 1
			if i == len(sorted_scores) or sorted_scores[i][1] < current_score[1] or sorted_scores[i][2] < 60:
				break
		result = []
		name_list = []
		for match in best_matches:
			if some_list[match][0].text in name_list:
				continue
			name_list.append(some_list[match][0].text)
			result.append(Parser.format(some_list[match]))
		return result

	@staticmethod
	def __create_list(files):
		item_list = []
		for file in files:
			root = ET.parse(file).getroot()
			for item in root:
				item_list.append(item)
		return item_list

	@staticmethod
	def format(xml_object):
		string = ""
		for element in xml_object:
			if len(list(element.iter())) > 1:
				string += Parser.format_sub(element)
				continue
			if element.tag is None or element.text is None:
				continue
			if element.tag == "roll":
				continue
			if element.tag == "text":
				string += "\n" + element.text + "\n"
				continue
			string += "*" + element.tag + ":* " + element.text + "\n"
		return string

	@staticmethod
	def format_sub(element):
		string = "\n*" + element.tag + "*:\n"
		for item in element:
			if item.tag is None or item.text is None:
				continue
			if item.tag == "name":
				string += "_" + item.text + "_: "
			else:
				string += item.text + "\n"
		return string
