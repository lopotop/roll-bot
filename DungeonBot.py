import logging
import random

from telegram import InlineQueryResultArticle, InputTextMessageContent


# from start import parser


class DungeonBot:
	logger = logging.getLogger()
	dice_types = ["d20", "d12", "d10", "d8", "d6", "d4"]

	def __init__(self, parser):
		self.parser = parser

	def __run_parser(self, bot, update, item_list):
		msg = self.__get_message(update.message.text)
		if not msg:
			bot.send_message(chat_id=update.message.chat_id, text="Your search query is empty!")
			return
		items = self.parser.find(self.__get_message(update.message.text), item_list)
		for i in range(len(items)):
			bot.send_message(chat_id=update.message.chat_id, text=str(items[i]), parse_mode='Markdown')
			if i >= 4:
				bot.send_message(chat_id=update.message.chat_id,
				                 text="There are more results, but I won't show them to you.\nTry to use more specific query.")
				return

	def spell(self, bot, update):
		self.__run_parser(bot, update, self.parser.spells)

	def item(self, bot, update):
		self.__run_parser(bot, update, self.parser.items)

	def monster(self, bot, update):
		self.__run_parser(bot, update, self.parser.monsters)

	def roll(self, bot, update):
		bot.send_message(chat_id=update.message.chat_id, text=self.__roll(update.message.text))

	def inline_roll(self, bot, update):
		query = update.inline_query.query
		if not query:
			return
		results = list()
		for i in range(len(self.dice_types)):
			results.append(InlineQueryResultArticle(id=str(i),
			                                        title=self.dice_types[i],
			                                        input_message_content=InputTextMessageContent(self.dice_types[i])))
		bot.answer_inline_query(update.inline_query.id, results)
		bot.send_message(chat_id=update.message.chat_id, text=self.__roll(update.message.text))

	@staticmethod
	def __roll(string):
		parts = string.split(" ")
		results = []
		if len(parts) == 1:
			parts = ["1d20"]
		for part in parts:
			if part.startswith('/roll'):
				continue
			tmps = part.split('d')
			try:
				dice_num = int(tmps[0])
			except:
				dice_num = 1
			try:
				dice_type = int(tmps[1])
			except:
				dice_type = 20
			if dice_num > 20:
				return "I don't have so many dices, sorry"
			for d in range(0, dice_num):
				results.append(random.randrange(0, dice_type) + 1)
			return results

	@staticmethod
	def __get_message(string):
		words = string.split(" ")
		result = ""
		for i in range(len(words)):
			if i == 0:
				continue
			if i == 1:
				result += words[i]
			else:
				result += " " + words[i]
		return result
