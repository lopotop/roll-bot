#!/usr/bin/python3

import os
import socket
import atexit
import logging

import sys
import threading

import schedule
import time

from DungeonBot import DungeonBot
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import InlineQueryHandler

from Parser import Parser
from Repos import Repos

debug = True

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger()


def __do_the_jobs():
	while True:
		logger.debug("Checking schedule")
		schedule.run_pending()
		time.sleep(15)


host = os.environ.get('HOST', '')
port = int(os.environ.get('PORT', '8443'))
token = os.environ.get('TOKEN', '')

if not token:
	logger.critical("Environment variables not set!")
	# TODO: Parse bot.conf file
	sys.exit()

if socket.gethostname() == host:
	debug = False

webhook_path = 'https://' + host + "/bot/" + token
updater = Updater(token=token)
atexit.register(updater.stop)
dispatcher = updater.dispatcher

if debug:
	logger.info("Starting in debug mode...")
	logger.setLevel(level=logging.DEBUG)
	updater.start_polling()
else:
	logger.info("Starting in server mode...")
	updater.start_webhook(listen="127.0.0.1",
	                      port=port,
	                      url_path="bot/" + token)
	updater.bot.set_webhook(webhook_path)

parser = Parser()
dungeonbot = DungeonBot(parser)
repos = Repos(parser.update)
thread = threading.Thread(target=__do_the_jobs)
thread.start()
dispatcher.add_handler(CommandHandler('roll', dungeonbot.roll))
dispatcher.add_handler(CommandHandler('spell', dungeonbot.spell))
dispatcher.add_handler(CommandHandler('item', dungeonbot.item))
dispatcher.add_handler(CommandHandler('mm', dungeonbot.monster))
# dispatcher.add_handler(InlineQueryHandler(dungeonbot.inline_roll))
logger.info("Bot started")
